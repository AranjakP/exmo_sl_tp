/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp.classes;

import java.util.Map;

/**
 *
 * @author user
 */
public class UserInfo {
    private String uid;
    private String server_date;
    private Map balances;
    private Map reserved;

    public UserInfo(String uid, String server_date, Map balances, Map reserved) {
        this.uid = uid;
        this.server_date = server_date;
        this.balances = balances;
        this.reserved = reserved;
    }

    public Map getReserved() {
        return reserved;
    }

    public void setReserved(Map reserved) {
        this.reserved = reserved;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getServer_date() {
        return server_date;
    }

    public void setServer_date(String server_date) {
        this.server_date = server_date;
    }

    public Map getBalances() {
        return balances;
    }

    public void setBalances(Map balances) {
        this.balances = balances;
    }
    
    
}
