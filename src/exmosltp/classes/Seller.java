/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp.classes;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class Seller {
    private SimpleStringProperty pair;
    private SimpleStringProperty sl_qty;
    private SimpleStringProperty sl_price;
    private SimpleStringProperty sl_if;
    private SimpleStringProperty sl_tr;
    private SimpleStringProperty tp_qty;
    private SimpleStringProperty tp_price;
    private SimpleStringProperty tp_if;
    private SimpleStringProperty tp_tr;
    private SimpleStringProperty max_price;
    private String tr_sl_price;
    private String start_price;
    private Boolean tp_tr_active;
    private Boolean sl_tr_active;
    private SimpleBooleanProperty done;

    public Seller(String pair, String sl_qty, String sl_price, String sl_if, String sl_tr, String tp_qty, String tp_price, String tp_if, String tp_tr) {
        this.pair = new SimpleStringProperty(pair);
        this.sl_qty = new SimpleStringProperty(sl_qty);
        this.sl_price = new SimpleStringProperty(sl_price);
        this.sl_if = new SimpleStringProperty(sl_if);
        this.sl_tr = new SimpleStringProperty(sl_tr);
        this.tp_qty = new SimpleStringProperty(tp_qty);
        this.tp_price = new SimpleStringProperty(tp_price);
        this.tp_if = new SimpleStringProperty(tp_if);
        this.tp_tr = new SimpleStringProperty(tp_tr);
        this.done = new SimpleBooleanProperty(false);
        this.start_price = "";
        this.tr_sl_price = "";
        this.tp_tr_active = false;
        this.sl_tr_active = false;
    }
    
    public SimpleStringProperty getPairProperty() {
        return pair;
    }

    public void setPair(SimpleStringProperty pair) {
        this.pair = pair;
    }

    public SimpleStringProperty getSl_qtyProperty() {
        return sl_qty;
    }

    public void setSl_qty(SimpleStringProperty sl_qty) {
        this.sl_qty = sl_qty;
    }

    public SimpleStringProperty getSl_priceProperty() {
        return sl_price;
    }

    public void setSl_price(SimpleStringProperty sl_price) {
        this.sl_price = sl_price;
    }

    public SimpleStringProperty getSl_ifProperty() {
        return sl_if;
    }

    public void setSl_if(SimpleStringProperty sl_if) {
        this.sl_if = sl_if;
    }
    
    public SimpleStringProperty getSl_trProperty() {
        return sl_tr;
    }

    public void setSl_tr(SimpleStringProperty sl_tr) {
        this.sl_tr = sl_tr;
    }

    public SimpleStringProperty getTp_qtyProperty() {
        return tp_qty;
    }

    public void setTp_qty(SimpleStringProperty tp_qty) {
        this.tp_qty = tp_qty;
    }

    public SimpleStringProperty getTp_priceProperty() {
        return tp_price;
    }

    public void setTp_price(SimpleStringProperty tp_price) {
        this.tp_price = tp_price;
    }

    public SimpleStringProperty getTp_ifProperty() {
        return tp_if;
    }

    public void setTp_if(SimpleStringProperty tp_if) {
        this.tp_if = tp_if;
    }
    
    public void setMax_price(SimpleStringProperty max_price) {
        this.max_price = max_price;
    }
    
    public SimpleStringProperty getTp_trProperty() {
        return tp_tr;
    }
    
    public SimpleStringProperty getMax_priceProperty() {
        return max_price;
    }

    public void setTp_tr(SimpleStringProperty tp_tr) {
        this.tp_tr = tp_tr;
    }
    
    public void setPair(String pair) {
        this.pair.set(pair);
    }
    
    public void setSl_qty(String sl_qty) {
        this.sl_qty.set(sl_qty);
    }
    
    public void setSl_price(String sl_price) {
        this.sl_price.set(sl_price);
    }
    
    public void setSl_if(String sl_if) {
        this.sl_if.set(sl_if);
    }
    
    public void setSl_tr(String sl_tr) {
        this.sl_tr.set(sl_tr);
    }
    
    public void setTp_qty(String tp_qty) {
        this.tp_qty.set(tp_qty);
    }
    
    public void setTp_price(String tp_price) {
        this.tp_price.set(tp_price);
    }
    
    public void setTp_if(String tp_if) {
        this.tp_if.set(tp_if);
    }
    
    public void setTp_tr(String tp_tr) {
        this.tp_tr.set(tp_tr);
    }
    
    public void setMax_price(String max_price) {
        this.max_price.set(max_price);
    }
    
    public void setTr_sl_price(String tr_sl_price) {
        this.tr_sl_price = tr_sl_price;
    }
    
    public void setStart_price(String start_price) {
        this.start_price = start_price;
    }
    
    public String getPair() {
        return pair.get();
    }
    
    public String getSl_qty() {
        return sl_qty.get();
    }

    public String getSl_price() {
        return sl_price.get();
    }

    public String getSl_if() {
        return sl_if.get();
    }
    
    public String getSl_tr() {
        return sl_tr.get();
    }

    public String getTp_qty() {
        return tp_qty.get();
    }

    public String getTp_price() {
        return tp_price.get();
    }

    public String getTp_if() {
        return tp_if.get();
    }
    
    public String getTp_tr() {
        return tp_tr.get();
    }
    
    public String getMax_price() {
        return max_price.get();
    }
    
    public String getTr_sl_price() {
        return tr_sl_price;
    }
    
    public String getStart_price() {
        return start_price;
    }

    public SimpleBooleanProperty getDoneProperty() {
        return done;
    }

    public void setDoneProperty(SimpleBooleanProperty done) {
        this.done = done;
    }
    
    public Boolean getDone() {
        return done.get();
    }
    
    public void setDone(Boolean done) {
        this.done.set(done);
    }
    
//    public SimpleBooleanProperty getTp_tr_activeProperty() {
//        return tp_tr_active;
//    }

//    public void setTp_tr_activeProperty(SimpleBooleanProperty tp_tr_active) {
//        this.done = tp_tr_active;
//    }
    
    public Boolean getTp_tr_active() {
        return tp_tr_active;
    }
    
    public void setTp_tr_active(Boolean tp_tr_active) {
        this.tp_tr_active= tp_tr_active;
    }
    public Boolean getSl_tr_active() {
        return sl_tr_active;
    }
    
    public void setSl_tr_active(Boolean sl_tr_active) {
        this.sl_tr_active = sl_tr_active;
    }
}
