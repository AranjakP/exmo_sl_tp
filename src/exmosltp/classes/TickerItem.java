/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp.classes;

/**
 *
 * @author user
 */
public class TickerItem {

    private Double buy_price;
    private Double sell_price;
    private Double last_trade;
    private Double high;
    private Double low;
    private Double avg;
    private Double vol;
    private Double vol_curr;

    public TickerItem(Double buy_price, Double sell_price, Double last_trade, Double high, Double low, Double avg, Double vol, Double vol_curr) {
        this.buy_price = buy_price;
        this.sell_price = sell_price;
        this.last_trade = last_trade;
        this.high = high;
        this.low = low;
        this.avg = avg;
        this.vol = vol;
        this.vol_curr = vol_curr;
    }

    
    
    public Double getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(Double buy_price) {
        this.buy_price = buy_price;
    }

    public Double getSell_price() {
        return sell_price;
    }

    public void setSell_price(Double sell_price) {
        this.sell_price = sell_price;
    }

    public Double getLast_trade() {
        return last_trade;
    }

    public void setLast_trade(Double last_trade) {
        this.last_trade = last_trade;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getAvg() {
        return avg;
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public Double getVol() {
        return vol;
    }

    public void setVol(Double vol) {
        this.vol = vol;
    }

    public Double getVol_curr() {
        return vol_curr;
    }

    public void setVol_curr(Double vol_curr) {
        this.vol_curr = vol_curr;
    }

}
