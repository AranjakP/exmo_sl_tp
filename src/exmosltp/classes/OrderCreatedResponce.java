/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp.classes;

/**
 *
 * @author user
 */
public class OrderCreatedResponce {
    private Boolean result;
    private String error;
    private String order_id;

    public OrderCreatedResponce(Boolean result, String error, String order_id) {
        this.result = result;
        this.error = error;
        this.order_id = order_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }   
    
}
