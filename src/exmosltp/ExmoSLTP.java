/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author user
 */
public class ExmoSLTP extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Preloader.fxml"));
        Parent root = loader.load();
        PreloaderController controller = loader.getController();
        controller.setHostServices(getHostServices());

        Scene scene = new Scene(root);
        stage.setTitle("CC2LS Exmo SLTP v0.7");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setOpacity(0.8);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
