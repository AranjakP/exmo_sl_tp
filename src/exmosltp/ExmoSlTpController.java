/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exmosltp;

import exmosltp.classes.Exmo;
import exmosltp.classes.C2LSServerResponce;
import exmosltp.classes.OrderCreatedResponce;
import exmosltp.classes.PairSettings;
import exmosltp.classes.UserInfo;
import exmosltp.classes.TickerItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import exmosltp.classes.Seller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author user
 */
public class ExmoSlTpController implements Initializable {

    Properties prop = new Properties();
    InputStream input = null;
    FileOutputStream output = null;
    String strCoinBalance, key, secret, ref_code, alert_to_client = "", message_to_clients = "";
    Exmo Exmo;
    public Gson gson = new Gson();
    Timeline stopLoss_TakeProfit_Handler, ApplicationServerConnection;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.UK);
    DecimalFormat df = (DecimalFormat) nf;
    private ObservableList<String> pairs = FXCollections.observableArrayList();
    Map<String, PairSettings> pairSettings;
    Double lastTradePrice = 0.0;
    Map<String, String> balanceItems;
    private ObservableList<Seller> Sellers = FXCollections.observableArrayList();
    Boolean isPairEdit = false;
    Boolean premium = false;

    @FXML
    private Label coinBalance;
    @FXML
    private TextField sl_qty;
    @FXML
    private TextField sl_price;
    @FXML
    private TextField sl_if;
    @FXML
    private TextField sl_tr;
    @FXML
    private TextField tp_qty;
    @FXML
    private TextField tp_price;
    @FXML
    private TextField tp_if;
    @FXML
    private TextField tp_tr;
    @FXML
    private TextArea app_log;

    @FXML
    private Button startButton;
    @FXML
    private Button stopButton;
    @FXML
    private Button addButton;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private HBox inputsHbox;
    @FXML
    private AnchorPane inputsAnchorPane;
    @FXML
    private AnchorPane tpAnchorPane;
    @FXML
    ProgressBar progressBar;
    @FXML
    TableView<Seller> sellersTable;
    @FXML
    TableColumn<Seller, String> pairColumn;
    @FXML
    TableColumn<Seller, String> sl_qtyColumn;
    @FXML
    TableColumn<Seller, String> sl_priceColumn;
    @FXML
    TableColumn<Seller, String> sl_ifColumn;
    @FXML
    TableColumn<Seller, String> sl_trColumn;
    @FXML
    TableColumn<Seller, String> tp_qtyColumn;
    @FXML
    TableColumn<Seller, String> tp_priceColumn;
    @FXML
    TableColumn<Seller, String> tp_ifColumn;
    @FXML
    TableColumn<Seller, String> tp_trColumn;

    @FXML
    private ChoiceBox pairChoiceBox;

    //lang
    @FXML
    private ChoiceBox choiceLang;
    private ResourceBundle bundle;
    private Locale locale;
    private final ObservableList<String> langList = FXCollections.observableArrayList("EN", "RU", "UA");
    @FXML
    private Label labelPair;
    @FXML
    private Label labelBalance;
    @FXML
    private Label labelStopLoss;
    @FXML
    private Label labelTakeProfit;
    @FXML
    private Label labelSellQtySl;
    @FXML
    private Label labelSellQtyTP;
    @FXML
    private Label labelSellPriceSL;
    @FXML
    private Label labelSellPriceTP;
    @FXML
    private Label labelSellIfSL;
    @FXML
    private Label labelSellIfTP;
    @FXML
    private Label labelTrailingSL;
    @FXML
    private Label labelTrailingTP;

    String lastTradePriceText, keyNeededText, secretNeededText, allFieldsNeeded, wrongNumberFormat, stopLossPriceWarning, notEnoughFunds;
    String applicationWillBeClosed, confirmStartProcess, beSure, availableBalance, orderCreated, cancel, buttonStartText;
    String forCoin, forPair, minQty, maxQty, minPrice, maxPrice, saveButtonText, editButtonText, selectPairText;
    String initialLang, one_instance, trial_alert, not_premium, old_version, user_not_found, access_disabled;
    private static final int PORT = 12345;
    private static ServerSocket s;
    String version = "0.7";
    String title = "CC2LS Exmo SLTP";
    String base_url = "https://cc2ls.io/api/exmosltp/";
    
    @FXML
    Hyperlink instagramHyperlink;
    @FXML
    Hyperlink telegramHyperlink;
    @FXML
    Hyperlink twitterHyperlink;
    @FXML
    Hyperlink wwwHyperlink;
    private HostServices hostServices;

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices ;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (check_load_api_settings()) {
            if (connectC2LSServer()) {
                fill_pairs();
                getPairSettings();

                this.stopLoss_TakeProfit_Handler = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            String result = Exmo.Request("ticker", null);
                            if (Exmo.is_success(result)) {
                                Map<String, TickerItem> mapTickerItems = new Gson().fromJson(result, new TypeToken<Map<String, TickerItem>>() {
                                }.getType());

//                              https://codereview.stackexchange.com/questions/64011/removing-elements-on-a-list-while-iterating-through-it
                                Iterator<Seller> it = Sellers.iterator();
                                while (it.hasNext()) {
                                    Seller seller = it.next();
                                    String pair = seller.getPair();
                                    TickerItem tickerItem = mapTickerItems.get(pair);
                                    lastTradePrice = tickerItem.getLast_trade();
                                    Double Sl_tr = Double.parseDouble(seller.getSl_tr());
                                    Double Tp_tr = Double.parseDouble(seller.getTp_tr());
                                    
                                    if (premium && (Sl_tr>0 || Tp_tr>0)) {
                                        if (Sl_tr>0) {
                                            if ("" == seller.getStart_price()) {
                                                seller.setStart_price(String.valueOf(lastTradePrice));
                                                System.out.println("Setup start price " + seller.getStart_price());
                                            }
                                            Double start_price = Double.parseDouble(seller.getStart_price());
                                            if (lastTradePrice>=start_price*(1+Sl_tr/100)) {
                                                seller.setTr_sl_price(String.valueOf(Double.parseDouble(seller.getSl_price())*(1+Sl_tr/100)));
                                                seller.setStart_price(String.valueOf(lastTradePrice));
                                                seller.setSl_tr_active(true);
//                                                System.out.println("Sl trailing activated ");
                                            } 
                                            
                                            if (seller.getSl_tr_active()) {
                                                if (lastTradePrice <= Double.parseDouble(seller.getTr_sl_price())) {
//                                                    System.out.println("SL trailing create order");
//                                                    System.out.println(lastTradePrice);
//                                                    System.out.println(Double.parseDouble(seller.getTr_sl_price()));
//                                                    createOrder(seller.getTr_sl_price(), seller.getSl_qty(), pair, "StopLoss", seller);
                                                    createOrder(String.valueOf(lastTradePrice), seller.getSl_qty(), pair, "StopLoss", seller);
                                                    it.remove();
                                                    playDone("lost");
                                                    get_balance();
                                                }
                                            } else {
                                                if (lastTradePrice <= Double.parseDouble(seller.getSl_if())) {
                                                    createOrder(seller.getSl_price(), seller.getSl_qty(), pair, "StopLoss", seller);
                                                    it.remove();
                                                    playDone("lost");
                                                    get_balance();
                                                }
                                            }  
                                        } else {
                                            if (lastTradePrice <= Double.parseDouble(seller.getSl_if())) {
                                                createOrder(seller.getSl_price(), seller.getSl_qty(), pair, "StopLoss", seller);
                                                it.remove();
                                                playDone("lost");
                                                get_balance();
                                            }
                                        }
                                        if (Tp_tr>0) {
                                            if (seller.getTp_tr_active()) {
                                                Double max_price = Double.parseDouble(seller.getMax_price());
//                                                System.out.println(max_price*(1-Tp_tr/100));
                                                if (lastTradePrice<=max_price*(1-Tp_tr/100)) {
//                                                    System.out.println("TP trailing create order");
//                                                    System.out.println(lastTradePrice);
//                                                    System.out.println(max_price*(1-Tp_tr/100));
//                                                    createOrder(String.valueOf(max_price*(1-Tp_tr/100)), seller.getTp_qty(), pair, "TakeProfit", seller);
                                                    createOrder(String.valueOf(lastTradePrice), seller.getTp_qty(), pair, "TakeProfit", seller);
                                                    it.remove();
                                                    playDone("profit");
                                                    get_balance();
                                                }   
                                            } else {
                                                if (lastTradePrice >= Double.parseDouble(seller.getTp_if())) {
                                                    seller.setTp_tr_active(true);
                                                    seller.setMax_price(lastTradePrice.toString());
//                                                    System.out.println("TP trailing activated");
                                                    sendEmailTrailing(pair);   
                                                }           
                                            }
                                        } else {
                                            if (lastTradePrice >= Double.parseDouble(seller.getTp_if())) {
                                                createOrder(seller.getTp_price(), seller.getTp_qty(), pair, "TakeProfit", seller);
                                                it.remove();
                                                playDone("profit");
                                                get_balance();
                                            }
                                        }

                                    } else {
                                        if (lastTradePrice <= Double.parseDouble(seller.getSl_if())) {
                                            createOrder(seller.getSl_price(), seller.getSl_qty(), pair, "StopLoss", seller);
                                            it.remove();
                                            playDone("lost");
                                            get_balance();
                                        }
                                        if (lastTradePrice >= Double.parseDouble(seller.getTp_if())) {
                                            createOrder(seller.getTp_price(), seller.getTp_qty(), pair, "TakeProfit", seller);
                                            it.remove();
                                            playDone("profit");
                                            get_balance();
                                        }
                                    }
                                }
                                if (Sellers.isEmpty()) {
                                    stopAction();
                                }
                            } else {
                                logger(Exmo.getError().getError() + " \n");
                            }
                        } catch (Exception ex) {
                            logger(ex.getMessage() + " \n");
//                            ex.printStackTrace();
                        }
                    }
                }));
                stopLoss_TakeProfit_Handler.setCycleCount(Timeline.INDEFINITE);

                ApplicationServerConnection = new Timeline(new KeyFrame(Duration.seconds(600), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        connectC2LSServer();
                    }
                }));
                ApplicationServerConnection.setCycleCount(Timeline.INDEFINITE);

                pairColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("pair"));
                sl_qtyColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("sl_qty"));
                sl_priceColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("sl_price"));
                sl_ifColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("sl_if"));
                sl_trColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("sl_tr"));
                tp_qtyColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("tp_qty"));
                tp_priceColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("tp_price"));
                tp_ifColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("tp_if"));
                tp_trColumn.setCellValueFactory(new PropertyValueFactory<Seller, String>("tp_tr"));
                sellersTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                sellersTable.setItems(Sellers);
                
                hyperlinksInit();
            }
        }
    }

    @FXML
    private void handleStartAction(ActionEvent event) {
//        if (checkForPairSettings() && confirmation_from_user()) {
        if (confirmation_from_user()) {
            startProcessing();
            startButton.setDisable(true);
            addButton.setDisable(true);
            editButton.setDisable(true);
            deleteButton.setDisable(true);
            stopButton.setDisable(false);
            pairChoiceBox.setDisable(true);
//            inputsHbox.setDisable(true);
            tpAnchorPane.setDisable(true);
            inputsAnchorPane.setDisable(true);
            addButton.setDisable(true);
            editButton.setDisable(true);
            deleteButton.setDisable(true);
        }
    }

    @FXML
    private void handleStopAction(ActionEvent event) {
        stopAction();
    }

    @FXML
    private void addPairPressed(ActionEvent event) {
        
        if (checkForPairSettings()) {
            String pair = pairChoiceBox.getSelectionModel().getSelectedItem().toString();
            Sellers.add(new Seller(pair, sl_qty.getText(), sl_price.getText(), sl_if.getText(), sl_tr.getText(), tp_qty.getText(), tp_price.getText(), tp_if.getText(), tp_tr.getText()));
            clearFields();
            pairChoiceBox.getSelectionModel().selectFirst();
            if (Sellers.size() > 0) {
                editButton.setDisable(false);
                deleteButton.setDisable(false);
                startButton.setDisable(false);
            }
            if (!premium && Sellers.size() >= 1) {
                message_to_user(not_premium);
                addButton.setDisable(true);
                return;
            }
        }
    }

    @FXML
    private void editPairPressed(ActionEvent event) {
        Seller selectedSeller = (Seller) sellersTable.getSelectionModel().getSelectedItem();
        if (sellerIsSelected(selectedSeller)) {
            if (isPairEdit && checkForPairSettings()) {
                isPairEdit = false;
                editButton.setText(editButtonText);
                addButton.setDisable(false);
                deleteButton.setDisable(false);
                pairChoiceBox.setDisable(false);
                sellersTable.setDisable(false);
                startButton.setDisable(false);
                selectedSeller.setSl_qty(sl_qty.getText());
                selectedSeller.setSl_price(sl_price.getText());
                selectedSeller.setSl_if(sl_if.getText());
                selectedSeller.setSl_tr(sl_tr.getText());
                selectedSeller.setTp_qty(tp_qty.getText());
                selectedSeller.setTp_price(tp_price.getText());
                selectedSeller.setTp_if(tp_if.getText());
                selectedSeller.setTp_tr(tp_tr.getText());
                clearFields();
                sellersTable.refresh();
            } else {
                pairChoiceBox.setValue(selectedSeller.getPair());
                sl_qty.setText(selectedSeller.getSl_qty());
                sl_price.setText(selectedSeller.getSl_price());
                sl_if.setText(selectedSeller.getSl_if());
                sl_tr.setText(selectedSeller.getSl_tr());
                tp_qty.setText(selectedSeller.getTp_qty());
                tp_price.setText(selectedSeller.getTp_price());
                tp_if.setText(selectedSeller.getTp_if());
                tp_tr.setText(selectedSeller.getTp_tr());

                isPairEdit = true;
                editButton.setText(saveButtonText);
                addButton.setDisable(true);
                deleteButton.setDisable(true);
                pairChoiceBox.setDisable(true);
                sellersTable.setDisable(true);
                startButton.setDisable(true);
            }
        }
        if (!premium) {
            sl_tr.setDisable(true);
            tp_tr.setDisable(true);
        }
    }

    @FXML
    private void deletePairPressed(ActionEvent event) {
        Seller selectedSeller = (Seller) sellersTable.getSelectionModel().getSelectedItem();
        if (sellerIsSelected(selectedSeller)) {
            Sellers.remove(selectedSeller);
            clearFields();
            pairChoiceBox.getSelectionModel().selectFirst();
            if (Sellers.size() <= 0) {
                editButton.setDisable(true);
                deleteButton.setDisable(true);
                startButton.setDisable(true);
            }
        }
    }

    private boolean sellerIsSelected(Seller selectedSeller) {
        if (selectedSeller == null) {
//            DialogManager.showInfoDialog(resourceBundle.getString("error"), resourceBundle.getString("select_person"));
            message_to_user(selectPairText);
            return false;
        }
        return true;
    }

    public void stopAction() {
        stopLoss_TakeProfit_Handler.stop();
        ApplicationServerConnection.stop();
//        Platform.runLater(() -> {
        progressBar.setProgress(0);
        progressBar.setVisible(false);
//        });
        addButton.setDisable(false);
        stopButton.setDisable(true);
        pairChoiceBox.setDisable(false);
//        inputsHbox.setDisable(false);
        tpAnchorPane.setDisable(false);
        inputsAnchorPane.setDisable(false);
        addButton.setDisable(false);

        if (Sellers.size() <= 0) {
            editButton.setDisable(true);
            deleteButton.setDisable(true);
            startButton.setDisable(true);
        } else {
            editButton.setDisable(false);
            deleteButton.setDisable(false);
            startButton.setDisable(false);
        }
        
        if (!premium) {
            sl_tr.setDisable(true);
            tp_tr.setDisable(true);
        }

    }

    public boolean check_load_api_settings() {
        try {

            input = new FileInputStream("settings.properties");

            prop.load(input);

            if (!prop.containsKey("lang") || prop.getProperty("lang").isEmpty() || prop.getProperty("lang") == null) {
                this.initialLang = "EN";
            } else {
                this.initialLang = prop.getProperty("lang");
            }
            initLang();

            check_date();
            check_running_application();

            if (prop.getProperty("key").isEmpty() || prop.getProperty("key") == null) {
                alert_to_user(keyNeededText);
                return false;
            }
            if (prop.getProperty("secret").isEmpty() || prop.getProperty("secret") == null) {
                alert_to_user(secretNeededText);
                return false;
            }

            this.Exmo = new Exmo(prop.getProperty("key"), prop.getProperty("secret"));
            this.key = prop.getProperty("key");
            this.ref_code = prop.getProperty("ref_code");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public boolean checkForPairSettings() {
        String pair = pairChoiceBox.getSelectionModel().getSelectedItem().toString();
        PairSettings currentPairSettings = pairSettings.get(pair);
        if (sl_qty.getText().trim().isEmpty() || tp_qty.getText().trim().isEmpty() || sl_qty.getText().trim().isEmpty() || tp_qty.getText().trim().isEmpty() || sl_price.getText().trim().isEmpty() || tp_price.getText().trim().isEmpty() || tp_if.getText().trim().isEmpty() || sl_if.getText().trim().isEmpty()) {
            alert_to_client = allFieldsNeeded;
            logger(alert_to_client + "\n");
            message_to_user(alert_to_client);
            return false;
        }
        if (!sl_qty.getText().matches("[0-9.]*")) {
            alert_to_client = wrongNumberFormat;
            logger(alert_to_client + "\n");
            message_to_user(alert_to_client);
            return false;
        }
        if (Double.parseDouble(sl_price.getText()) > Double.parseDouble(sl_if.getText())) {
            alert_to_client = stopLossPriceWarning;
//            logger(alert_to_client);
            message_to_user(alert_to_client);
        }

        if (Double.parseDouble(currentPairSettings.getMin_quantity()) > Double.parseDouble(sl_qty.getText())
                || Double.parseDouble(currentPairSettings.getMin_quantity()) > Double.parseDouble(tp_qty.getText())) {
            alert_to_client = forCoin + " " + pair.substring(0, pair.indexOf("_")) + " " + minQty + " " + currentPairSettings.getMin_quantity() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;
        } else if (Double.parseDouble(currentPairSettings.getMax_quantity()) < Double.parseDouble(sl_qty.getText())
                || Double.parseDouble(currentPairSettings.getMax_quantity()) < Double.parseDouble(tp_qty.getText())) {
            alert_to_client = forCoin + " " + pair.substring(0, pair.indexOf("_")) + " " + maxQty + " " + currentPairSettings.getMax_quantity() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;
        } else if (Double.parseDouble(currentPairSettings.getMax_price()) < Double.parseDouble(sl_price.getText())
                || Double.parseDouble(currentPairSettings.getMax_price()) < Double.parseDouble(tp_price.getText())) {
            alert_to_client = forPair + " " + pair + " " + maxPrice + " " + currentPairSettings.getMax_price() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;
        } else if (Double.parseDouble(currentPairSettings.getMin_price()) > Double.parseDouble(sl_price.getText())
                || Double.parseDouble(currentPairSettings.getMin_price()) > Double.parseDouble(tp_price.getText())) {
            alert_to_client = forPair + " " + pair + " " + minPrice + " " + currentPairSettings.getMin_price() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;
            /*} else if (Double.parseDouble(currentPairSettings.getMin_amount()) > Double.parseDouble(sl_price.getText()) * Double.parseDouble(sl_qty.getText())
                || Double.parseDouble(currentPairSettings.getMin_amount()) > Double.parseDouble(tp_price.getText()) * Double.parseDouble(tp_qty.getText())) {
            alert_to_client = "Для пары " + pair + " минимальная сумма сделки " + currentPairSettings.getMin_amount() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;
        } else if (Double.parseDouble(currentPairSettings.getMax_amount()) < Double.parseDouble(sl_price.getText()) * Double.parseDouble(sl_qty.getText())
                || Double.parseDouble(currentPairSettings.getMax_amount()) < Double.parseDouble(tp_price.getText()) * Double.parseDouble(tp_qty.getText())) {
            alert_to_client = "Для пары " + pair + " максимальная сумма сделки " + currentPairSettings.getMax_amount() + "\n";
            logger(alert_to_client);
            message_to_user(alert_to_client);
            return false;*/
        } else if (Double.parseDouble(strCoinBalance) < Double.parseDouble(sl_qty.getText())
                || Double.parseDouble(strCoinBalance) < Double.parseDouble(tp_qty.getText())) {
            alert_to_client = notEnoughFunds;
            logger(alert_to_client + "\n");
            message_to_user(alert_to_client);
            return false;
        }
        return true;
    }

    public void alert_to_user(String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(message);
        String s = applicationWillBeClosed;
        alert.setContentText(s);
        alert.showAndWait();
        System.exit(0);
    }

    public void message_to_user(String message) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    public boolean confirmation_from_user() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title+". " + confirmStartProcess);
        alert.setHeaderText(beSure);

        ButtonType buttonTypeOne = new ButtonType(buttonStartText);
        ButtonType buttonTypeCancel = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return true;
        } else {
            return false;
        }
    }

    public void startProcessing() {
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> {
                    progressBar.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
                    progressBar.setVisible(true);
                });
                stopLoss_TakeProfit_Handler.play();
                return null;
            }
        };

        Thread th = new Thread(task);
        th.start();

        Task task2 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                ApplicationServerConnection.play();
                return null;
            }
        };

        Thread th2 = new Thread(task2);
        th2.start();
    }

    private boolean connectC2LSServer() {

        try {
            String url = base_url + "sltp/?key=" + ref_code + "&version=" + version + "&lang=" + choiceLang.getSelectionModel().getSelectedItem().toString();
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);
            HttpResponse httpResponse = client.execute(request);

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            StringBuffer resultBuffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                resultBuffer.append(line);
            }

            C2LSServerResponce serverResponce = gson.fromJson(resultBuffer.toString(), C2LSServerResponce.class);

//            if ("error".equals(serverResponce.getStatus()) && !"er00".equals(serverResponce.getError_code())) {
            if ("error".equals(serverResponce.getStatus())) {

                switch (serverResponce.getError_code()) {
                    case "er01":
                        alert_to_user(old_version);
                        return false;                        
                    case "er02":
                        alert_to_user(user_not_found);
                return false;
                    case "er05":
                        alert_to_user(access_disabled);
                        return false;
                    default:
                        break;
                }
                
            }

            if ("ok".equals(serverResponce.getStatus())) {
                if (!message_to_clients.equals(serverResponce.getMessage_to_clients()) && !"".equals(serverResponce.getMessage_to_clients())) {
                    message_to_clients = serverResponce.getMessage_to_clients();
                    logger(dtf.format(LocalDateTime.now()) + " - " + message_to_clients + "\n");
                }
                if ("yes".equals(serverResponce.getPremium())) {
                    premium = true;
                    sl_tr.setDisable(false);
                    tp_tr.setDisable(false);
                } else {
                    sl_tr.setDisable(true);
                    tp_tr.setDisable(true);
                }
            }
        } catch (IOException ex) {
            logger(ex.getMessage() + "\n");
        }
        return true;
    }

    private boolean sendEmailC2LSServer(String pair, String price, String sltp_action, Seller seller) {

        try {
            String url = base_url + "sltp_mail_done/?key=" + ref_code + "&pair=" + pair + "&price=" + price + "&lang=" + locale.toString() + "&sltp_action=" + sltp_action + "&sl_price="+seller.getSl_price()+"&sl_qty="+seller.getSl_qty()+"&sl_if="+seller.getSl_if()+"&tp_price="+seller.getTp_price()+"&tp_qty="+seller.getTp_qty()+"&tp_if="+seller.getTp_if();
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);
            HttpResponse httpResponse = client.execute(request);
        } catch (IOException ex) {
            logger(ex.getMessage() + "\n");
        }
        return true;
    }
    
    private void sendEmailTrailing(String pair) {
        Task task5 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    String url = base_url + "sltp_mail_trailing_activated/?key=" + ref_code + "&pair=" + pair + "&lang=" + locale.toString();
                    HttpClient client = HttpClientBuilder.create().build();
                    HttpGet request = new HttpGet(url);
                    HttpResponse httpResponse = client.execute(request);
                } catch (IOException ex) {
                    logger(ex.getMessage() + "\n");
                }
                return null;
            }
        };
        Thread th5 = new Thread(task5);
        th5.start();    
    }

    public void logger(String textLog) {
        Platform.runLater(() -> {
            app_log.appendText(textLog);
        });
    }

    public void fill_pairs() {
        String result = Exmo.Request("ticker", null);
        if (Exmo.is_success(result)) {
            Map<String, TickerItem> mapTickerItems = new Gson().fromJson(result, new TypeToken<Map<String, TickerItem>>() {
            }.getType());
            for (String ticker_key : mapTickerItems.keySet()) {
                pairs.add(ticker_key);
            }
            pairChoiceBox.setItems(pairs);
            pairChoiceBox.getSelectionModel().selectFirst();
            get_balance();
            pairChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (newValue.contains("USD") || newValue.contains("EUR") || newValue.contains("RUB") || newValue.contains("UAH") || newValue.contains("PLN")) {
                        nf.setMaximumFractionDigits(4);
                        nf.setMinimumFractionDigits(4);
                    } else {
                        nf.setMaximumFractionDigits(8);
                        nf.setMinimumFractionDigits(8);
                    }
                    get_balance();
                    clearFields();
                }
            });
        } else {
            logger(Exmo.getError().getError() + "\n");
        }
    }

    public void clearFields() {
        sl_qty.setText("");
        sl_price.setText("");
        sl_if.setText("");
        sl_tr.setText("0");
        tp_qty.setText("");
        tp_price.setText("");
        tp_if.setText("");
        tp_tr.setText("0");
    }

    public void getPairSettings() {
        String result = Exmo.Request("pair_settings", null);
        if (Exmo.is_success(result)) {
            pairSettings = new Gson().fromJson(result, new TypeToken<Map<String, PairSettings>>() {
            }.getType());
        } else {
            logger(Exmo.getError().getError() + "\n");
        }
    }

    public void get_balance() {
        String result = Exmo.Request("user_info", null);
        if (Exmo.is_success(result)) {
            UserInfo userInfo = new Gson().fromJson(result, UserInfo.class);

            if (!Objects.equals(userInfo.getBalances(), balanceItems)) {
                balanceItems = userInfo.getBalances();
                logger(availableBalance + "\n");
                for (String map_key : balanceItems.keySet()) {
                    String value = balanceItems.get(map_key);
                    if (!value.equals("0")) {
                        logger(map_key + ": " + value + "\n");
                    }
                }
            }

            String pair = pairChoiceBox.getSelectionModel().getSelectedItem().toString();
            if (pair.indexOf("_") >= 0) {
                String coin = pair.substring(0, pair.indexOf("_"));
                strCoinBalance = userInfo.getBalances().get(coin).toString();
                coinBalance.setText(strCoinBalance + " " + coin);
            }
        } else {
            logger(Exmo.getError().getError() + "\n");
            if ("Error 40017: Wrong api key".equals(Exmo.getError().getError())) {
                alert_to_user(Exmo.getError().getError());
            }
        }
    }

    public void createOrder(String orderPrice, String qty, String pair, String action, Seller seller) {
        Task task3 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                Map<String, String> params = new HashMap<String, String>();
                params.put("pair", pair);
                params.put("quantity", qty);
                params.put("price", orderPrice);
                params.put("type", "sell");

                String result = Exmo.Request("order_create", params);
                OrderCreatedResponce responce = new Gson().fromJson(result, OrderCreatedResponce.class);
                if (responce.getResult()) {
                    Platform.runLater(() -> {
                        logger("\n" + action + " " + pair + " " + orderCreated + " " + responce.getOrder_id() + "\n\n");
                    });
                    sendEmailC2LSServer(pair, orderPrice, action, seller);
                } else {
                    Platform.runLater(() -> {
                        logger(responce.getError() + "\n");
                    });
                }

                return null;
            }
        };
        Thread th3 = new Thread(task3);
        th3.start();
    }

    public void playDone(String action) {
        String musicFile = action + ".wav";
        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
    }

    public void changedSlQty() {
        if (tp_qty.getText().trim().isEmpty()) {
            tp_qty.setText(sl_qty.getText());
        }
    }

    public void changedTpQty() {
        if (sl_qty.getText().trim().isEmpty()) {
            sl_qty.setText(tp_qty.getText());
        }
    }

    public boolean update_lang_prop(String lang) {
        try {

            input = new FileInputStream("settings.properties");

            prop.load(input);

            prop.setProperty("lang", lang);

            output = new FileOutputStream("settings.properties");

            prop.store(output, null);

        } catch (IOException ex) {
            logger(ex.getMessage() + "\n");
        } finally {
            if (input != null) {
                try {
                    input.close();
                    output.close();
                } catch (IOException e) {
                    logger(e.getMessage() + "\n");
                }
            }
        }
        return true;
    }

    public void initLang() {
        choiceLang.setItems(langList);
        choiceLang.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.equals(oldValue)) {
                    setupTexts(newValue);
                    update_lang_prop(newValue);
                }
            }
        });

        if (!initialLang.isEmpty() && !"".equals(initialLang)) {
            setupTexts(initialLang);
            choiceLang.setValue(initialLang);

        } else {
            choiceLang.getSelectionModel().selectFirst();
        }
    }

    public void setupTexts(String newLocale) {
        locale = new Locale(newLocale);
        bundle = ResourceBundle.getBundle("exmosltp.lang.lang", locale);
        labelPair.setText(bundle.getString("labelPair"));
        labelBalance.setText(bundle.getString("labelBalance"));
        labelStopLoss.setText(bundle.getString("labelStopLoss"));
        labelTakeProfit.setText(bundle.getString("labelTakeProfit"));
        labelSellQtySl.setText(bundle.getString("labelSellQtySl"));
        labelSellQtyTP.setText(bundle.getString("labelSellQtyTP"));
        labelSellPriceSL.setText(bundle.getString("labelSellPriceSL"));
        labelSellPriceTP.setText(bundle.getString("labelSellPriceTP"));
        labelSellIfSL.setText(bundle.getString("labelSellIfSL"));
        labelSellIfTP.setText(bundle.getString("labelSellIfTP"));
        labelTrailingSL.setText(bundle.getString("trailing"));
        labelTrailingTP.setText(bundle.getString("trailing"));

        startButton.setText(bundle.getString("buttonStartText"));
        stopButton.setText(bundle.getString("buttonStopText"));

        lastTradePriceText = bundle.getString("lastTradePriceText");
        keyNeededText = bundle.getString("keyNeededText");
        secretNeededText = bundle.getString("secretNeededText");
        allFieldsNeeded = bundle.getString("allFieldsNeeded");
        wrongNumberFormat = bundle.getString("wrongNumberFormat");
        stopLossPriceWarning = bundle.getString("stopLossPriceWarning");
        notEnoughFunds = bundle.getString("notEnoughFunds");
        applicationWillBeClosed = bundle.getString("applicationWillBeClosed");
        confirmStartProcess = bundle.getString("confirmStartProcess");
        beSure = bundle.getString("beSure");
        availableBalance = bundle.getString("availableBalance");
        orderCreated = bundle.getString("orderCreated");
        cancel = bundle.getString("cancel");
        buttonStartText = bundle.getString("buttonStartText");
        forCoin = bundle.getString("forCoin");
        forPair = bundle.getString("forPair");
        minQty = bundle.getString("minQty");
        maxQty = bundle.getString("maxQty");
        minPrice = bundle.getString("minPrice");
        maxPrice = bundle.getString("maxPrice");

        addButton.setText(bundle.getString("addButton"));
        editButton.setText(bundle.getString("editButton"));
        editButtonText = bundle.getString("editButton");
        deleteButton.setText(bundle.getString("deleteButton"));
        saveButtonText = bundle.getString("saveButton");
        pairColumn.setText(bundle.getString("labelPair"));
        sl_qtyColumn.setText(bundle.getString("sl_qtyColumn"));
        sl_priceColumn.setText(bundle.getString("sl_priceColumn"));
        sl_ifColumn.setText(bundle.getString("sl_ifColumn"));
        sl_trColumn.setText(bundle.getString("trailing"));
        tp_qtyColumn.setText(bundle.getString("tp_qtyColumn"));
        tp_priceColumn.setText(bundle.getString("tp_priceColumn"));
        tp_ifColumn.setText(bundle.getString("tp_ifColumn"));
        tp_trColumn.setText(bundle.getString("trailing"));
        selectPairText = bundle.getString("selectPairText");
        one_instance = bundle.getString("one_instance");
        trial_alert = bundle.getString("trial_alert");
        not_premium = bundle.getString("not_premium");
        old_version = bundle.getString("old_version");
        user_not_found = bundle.getString("user_not_found");
        access_disabled = bundle.getString("access_disabled");

        sellersTable.setPlaceholder(new Label(bundle.getString("no_content")));
    }

    public void check_date() {
        long l = 1532948400000L; //https://www.fileformat.info/tip/java/date2millis.htm
        Date date = new Date(l);
        Date date2 = new Date(System.currentTimeMillis());
        if (date.before(date2)) {
            alert_to_user(trial_alert);
        }
    }

    public void check_running_application() {
        try {
            s = new ServerSocket(PORT, 10, InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            // shouldn't happen for localhost
        } catch (IOException e) {
            alert_to_user(one_instance);
        }
    }
    
    public void hyperlinksInit() {
        
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        
        instagramHyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                getHostServices().showDocument("https://www.instagram.com/cryptocurrency_tools/");
            }
        });
        
        telegramHyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                getHostServices().showDocument("https://t.me/joinchat/FG5tzxAe4vnIosnfzKdY_A");
            }
        });
        
        twitterHyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                getHostServices().showDocument("https://twitter.com/cc2ls/");
            }
        });
        
        wwwHyperlink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                getHostServices().showDocument("https://cc2ls.io");
            }
        });
    }
}
